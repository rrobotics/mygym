#!/usr/bin/env python3

import gym, os
import numpy as np
import random
from gym import Env
from gym.spaces import Discrete, Box
from gym import wrappers
from gym import utils
from gym.envs.mujoco import mujoco_env
import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Flatten, Input, Reshape, Rescaling, Activation, Concatenate, concatenate
from tensorflow.keras.optimizers import Adam
from rl.agents import DQNAgent, DDPGAgent, NAFAgent
from rl.policy import BoltzmannQPolicy
from rl.random import OrnsteinUhlenbeckProcess
from rl.memory import SequentialMemory

import mu_models

tf.config.set_visible_devices([], 'GPU')

def build_agent_2pendulum(num_action, observation_shape):
    last_init = tf.random_uniform_initializer(minval=-0.003, maxval=0.003)
    inputs = layers.Input(shape=(1,)+observation_shape)
    x = Flatten()(inputs)
    out = layers.Dense(256, activation="relu")(x)
    out = layers.Dense(256, activation="relu")(out)
    outputs = layers.Dense(num_action, activation="tanh", kernel_initializer=last_init)(out)
    outputs = outputs * upper_bound
    actor = tf.keras.Model(inputs, outputs)

    state_input = layers.Input(shape=(1,)+observation_shape)
    state_out = Flatten()(state_input)
    state_out = layers.Dense(16, activation="relu")(state_out)
    state_out = layers.Dense(32, activation="relu")(state_out)
    critic_action_input = layers.Input(shape=(num_action,))
    action_out = layers.Dense(32, activation="relu")(critic_action_input)
    concat = layers.Concatenate()([state_out, action_out])
    out = layers.Dense(256, activation="relu")(concat)
    out = layers.Dense(256, activation="relu")(out)
    outputs = layers.Dense(1)(out)
    critic = tf.keras.Model([state_input, critic_action_input], outputs)

    memory = SequentialMemory(limit=10**5, window_length=1)
    agent = DDPGAgent(
        num_action,
        actor,
        critic,
        critic_action_input,
        memory
    )
    # actor.summary()
    # critic.summary()
    return agent

def build_agent_pendulum(num_action, observation_shape):
    action_input = Input(shape=(1,)+observation_shape)
    x = Flatten()(action_input)
    x = Dense(16, activation="relu")(x)
    x = Dense(16, activation="relu")(x)
    x = Dense(num_action, activation="tanh")(x)
    actor = Model(inputs=action_input, outputs=x)

    critic_action_input = Input(shape=(num_action,))
    observation_input = Input(shape=(1,)+observation_shape)
    flattened_observation = Flatten()(observation_input)
    x_c = concatenate([critic_action_input, flattened_observation])
    x_c = Dense(32, activation="relu")(x_c)
    x_c = Dense(32, activation="relu")(x_c)
    x_c = Dense(1, activation="linear")(x_c)
    critic = Model(inputs=[critic_action_input, observation_input], outputs=x_c)

    memory = SequentialMemory(limit=10**5, window_length=1)
    agent = DDPGAgent(
        num_action,
        actor,
        critic,
        critic_action_input,
        memory
    )
    # actor.summary()
    # critic.summary()
    return agent

def watch(e, model):
    episodes = 10
    for episode in range(1, episodes+1):
        state = e.reset()
        done = False
        score = 0

        while not done:
            e.render()
            print(e.sim.data.qpos, e.sim.data.qvel)
            # state = np.expand_dims(state, axis=0)
            state = np.reshape(state, (1,1,state.shape[0]))
            # print(state.shape)
            action = model.predict(state) #e.action_space.sample()
            state, reward, done, info = e.step(action)
            score+=reward
        print('Episode:{} Score:{}'.format(episode, score))

env = gym.make('MyInvertedPendulum-v0')
# env = InvertedPendulumEnv()
upper_bound = env.action_space.high
lower_bound = env.action_space.low
model_name = "DDPGAgent.h5f"

# print("Size of State Space  ->\t{}".format(env.observation_space.shape))
# print("Size of Action Space ->\t{}".format(env.action_space.shape[0]))
# print("Max Value of Action  ->\t{}".format(upper_bound))
# print("Min Value of Action  ->\t{}".format(lower_bound))
# quit()

agent = build_agent_pendulum(env.action_space.shape[0], env.observation_space.shape)
agent.compile(Adam(learning_rate=1e-3, clipnorm=1.), metrics=["mae"])

# load the previously saved weights
agent.load_weights(os.path.join(os.path.dirname(__file__), os.path.join("models_weights", model_name)))

# hty = agent.fit(env, nb_steps=50000, visualize=False, verbose=1, nb_max_episode_steps=400)
# agent.save_weights(os.path.join("model_weights", model_name), overwrite=True)

# test the saved weights
agent.test(env, nb_episodes=2, visualize=True, nb_max_episode_steps=1000)

# watch(env, agent.actor)
